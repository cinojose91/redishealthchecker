# README #

This script will help to check the redis server memory usage every 5 minutes and alert if the memory usage is more than 2G for consecutive 15 minutes

To run the script

```

python memcheck.py

```