import time
import redis

REDIS_HOST = "127.0.0.1"
REDIS_PORT = "6379"
REDIS_PASSWORD = ""


def checkAndAlert(collection):
    alert = 0;
    print collection
    for i in collection:
        memb = int(i)/1000
        if memb > 2000:
            alert +=1
    if alert > 2:
        print "All hell broke loose!"

def redisChecker():
    r = redis.Redis(
    host=REDIS_HOST,
    port=REDIS_PORT, 
    password=REDIS_PASSWORD)
    collection = range(3)
    count = 0;
    while True:
        current = r.info()['used_memory']
        if count > 2:
           count = 0
           collection[count] = current 
        else:
           collection[count] = current
           count +=1
        checkAndAlert(collection)
        time.sleep(300)
    
def main():
    redisChecker()
    
if __name__ == "__main__":
   main()

